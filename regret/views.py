# -*- coding: utf-8 -*-
from __future__ import division

from otree.common import Currency as c, currency_range, safe_json

from . import models
from ._builtin import Page, WaitPage
import random
import time
from .models import Constants



class intro(Page):

    template_name = 'regret/IntroPage.html'
    #body_text = 'this is a test body text omglol'

    def vars_for_template(self):
        self.player.start_time = time.time()
        #self.player.start_time = time.strftime('%Y-%m-%d %H:%M')
        return {'header': "Welcome!", 'body_text': "This experiment is part of a study about economic decision \
                making. Please read the following instructions carefully, and \
                then make your choice.",'dbg':self.player.assigned_group,'assigned_group':self.player.assigned_group}

    '''
    def vars_for_template(self):
        return {'body_text': "This experiment is part of a study about economic decision \
                making. Please read the following instructions carefully, and \
                then make your choice, your group is:" + self.player.assigned_group,
                'more_body_text':
                    "In this study there are two players, an \"investor\" and his/her \"partner\".\
                    Your role in this game will be determined by choosing one of the two blank\
                    notes that now appear on your screen.",'dbg':self.player.assigned_group}
    '''



class roleExplained(Page):

    template_name = 'regret/RoleExplained.html'
    #body_text = 'this is a test body text omglol'

    def vars_for_template(self):
        return {'assigned_group':self.player.assigned_group,'body_text': "  You have been assigned the role of \"investor\". Another person who was assigned the role of \"partner\" is playing in parallel to you and his/her role will be explained shortly.",'dbg':self.player.assigned_group}

    pass

class startingMoney(Page):

    template_name = 'regret/InitialMoney.html'
    #body_text = 'this is a test body text omglol'

    def vars_for_template(self):
        if self.player.assigned_group == "C1" or self.player.assigned_group == "C2":
            return {'p1': "At the beginning of this experiment, you receive",
                    'total_sum': "5",
                    'p2': "which are shared evenly between yourself and your partner in the game; ",
                    'per_player': "2.5",
                    'p3': "dollar each.", 'dbg': self.player.assigned_group,
                    'assigned_group': self.player.assigned_group}
        else:
            return {'p1': "At the beginning of this game, you receive",
                'total_sum': "5",
                'p2': "which are shared evenly between yourself and your partner in the game; ",
                'per_player' : "2.5",
                'p3' : "dollar each.",'dbg':self.player.assigned_group,
                'assigned_group': self.player.assigned_group}

    pass



class chooseInvestor(Page):

    template_name = 'regret/ChooseInvestor.html'

    def vars_for_template(self):
        if self.player.assigned_group == 'B1':
            return {'p1': "In this study there are two players, an \"investor\" and his/her \"partner\".",
                'p2': "Your role in this game will be determined by choosing one of the two blank notes that "
                            "now appear on your screen.",
                'p3': "Please choose one of the options before continuing.",'dbg':self.player.assigned_group,'assigned_group':self.player.assigned_group}
        elif self.player.assigned_group == 'B2':
            return {'p1': "In this study there are two players, an \"investor\" and his/her \"partner\".",
                    'p2': "Your role in this game will be determined by choosing one of the two blank notes that "
                          "now appear on your screen.",
                    'p3': "Please choose one of the options before continuing.", 'dbg': self.player.assigned_group,'assigned_group':self.player.assigned_group}
        elif self.player.assigned_group == 'C1':
            return {'p1': "In this study there are two players, an \"investor\" and his/her \"partner\".",
                    'p2': "Your role in this game will be determined by choosing one of the two blank notes that "
                          "now appear on your screen.",
                    'p3': "Please choose one of the options before continuing.", 'dbg': self.player.assigned_group,'assigned_group':self.player.assigned_group}
        elif self.player.assigned_group == 'C2':
            return {'p1': "In this study there are two players, an \"investor\" and his/her \"partner\".",
                    'p2': "Your role in this game will be determined by choosing one of the two blank notes that "
                          "now appear on your screen.",
                    'p3': "Please choose one of the options before continuing.", 'dbg': self.player.assigned_group,'assigned_group':self.player.assigned_group}


class choiceExplanations(Page):

    template_name = 'regret/ExplanationOfChoice.html'

    def vars_for_template(self):
        if self.player.assigned_group == 'C2' or self.player.assigned_group == 'B2':
            return {'p1': "Next, you will be asked to choose between one of the two following options:",
                        'first_choice': "Keep the ",
                        'keep_amm': "5",
                        'second_choice': "Invest the $5 with a 50% chance of receiving $8 and a 50% chance of receiving",
                        'win_amm': " ",
                        'second_choice_cont': "$2",
                        'choice_summery': "In any case, the sum of money received will be divided evenly between you and your partner. "
                                          ,'dbg': self.player.assigned_group,'assigned_group':self.player.assigned_group}
        else:
            return {'p1': "Next, you will be asked to choose between one of the two following options:",
                'first_choice': "Keep the ",
                'keep_amm': "5",
                'second_choice': "Invest the $5 with a 50% chance of receiving $8 and a 50% chance of receiving",
                'win_amm': " ",
                    'assigned_group': self.player.assigned_group,
                'second_choice_cont': "$2",
                'choice_summery': "In any case, the sum of money received will be divided evenly between you and your partner.",'dbg':self.player.assigned_group}


class choiceCont(Page):

    template_name = 'regret/ExpOfChoiceCont.html'

    def vars_for_template(self):
        if self.player.assigned_group == 'C2' or self.player.assigned_group == 'C1':
            if self.player.assigned_group == "C1":
                return {
                    'dbg': self.player.assigned_group,
                    'assigned_group': self.player.assigned_group,
                'p1': "Next, if you choose to invest the initial sum, a raffle will be conducted and its results will be revealed to you and to your partner. ",
                'p2_a': "Based on this information, your partner will have the option to deduct $1 from your share, if s/he is not satisfied. Note that your partner will ",
                'under': "not receive",
                'p2_b': " the $1 if s/he chooses to deduct your share. S/he can only reduce your share."
                }
            else:
                return {
                    'dbg': self.player.assigned_group,
                    'assigned_group': self.player.assigned_group,
                    'p1': "Next, if you choose to invest the initial sum, a raffle will be conducted and its results will be revealed to you and to your partner. If you choose to invest the initial sum the results of the raffle will affect your payoff, and if you don't choose to invest it, the results of the raffle will not affect your payoff.",
                    'p2_a': "Based on this information, your partner will have the option to deduct $1 from your share, if s/he is not satisfied. Note that your partner will ",
                    'under': "not receive",
                    'p2_b': " the $1 if s/he chooses to deduct your share. S/he can only reduce your share."
                }
        else:
            if self.player.assigned_group == 'B1':
                return {
                    'dbg': self.player.assigned_group,
                     'assigned_group': self.player.assigned_group,
                    'p1': "After making your decision whether to keep or to invest the initial sum, your partner will be notified of your decision.",
                'p2_a': "Based on your decision, your partner will have the option to deduct $1 from your share, if s/he is not satisfied. Note that your partner will",
                'under':"not receive",
                'p2_b': " the $1 if s/he chooses to deduct your share. S/he can only reduce your share."
                }
            else:
                return {
                    'dbg': self.player.assigned_group,
                    'assigned_group': self.player.assigned_group,
                    'p1': "After making your decision whether to keep or to invest the initial sum, your partner will be notified of your decision.",
                    'p2_a': "Based on your decision, your partner will have the option to deduct $1 from your share, if s/he is not satisfied. Note that your partner will",
                    'under': "not receive",
                    'p2_b': " the $1 if s/he chooses to deduct your share. S/he can only reduce your share."
                    }

class choiceContTwo(Page):

    template_name = 'regret/ExpOfChoiceContTwo.html'

    def vars_for_template(self):
        if self.player.assigned_group == 'B1':
            return {
                'dbg':self.player.assigned_group,
                 'assigned_group': self.player.assigned_group,
                'p1': "Next, if you choose to invest the initial sum, a raffle will be conducted and its results will be revealed to you and to your partner.",
                'p2': "The final payoff to each side will be calculated based on the decision to invest/not-invest, the outcome of the raffle (if \"invest\" was chosen), and the partner's decision to deduct/not deduct the investor's share."
                }

        elif self.player.assigned_group == 'B2':
            return {
                'dbg': self.player.assigned_group,
                'assigned_group': self.player.assigned_group,
                'p1': "Next, a raffle will be conducted and its results will be revealed to you and to your partner. If you choose to invest the initial sum the results of the raffle will affect your payoff, and if you don't choose to invest it, the results of the raffle will not affect your payoff.",
                'p2': "The final payoff to each side will be calculated based on the decision to invest/not-invest, the outcome of the raffle (if \"invest\" was chosen), and the partner's decision to deduct/not deduct the investor's share."
                }
        elif self.player.assigned_group == 'C1':
            return {
                'dbg': self.player.assigned_group,
                'assigned_group': self.player.assigned_group,
                'p1': "The final payoff to each side will be calculated based on the decision to invest/not-invest, the outcome of the raffle (if \"invest\" was chosen), and the partner's decision to deduct/not deduct the investor's share.",
                }
        else:
            return {
                'dbg': self.player.assigned_group,
                'assigned_group': self.player.assigned_group,
                'p1': "The final payoff to each side will be calculated based on the decision to invest/not-invest, the outcome of the raffle (if \"invest\" was chosen), and the partner's decision to deduct/not deduct the investor's share.",
            }

class keepOrInvest(Page):

    template_name = 'regret/KeepOrInvest.html'
    form_model = models.Player
    form_fields = ['keep_or_invest']
    def vars_for_template(self):
        print self.player.assigned_group
        if self.player.assigned_group == 'B1' or self.player.assigned_group =='B2':
            keepOrInvest.template_name = 'regret/KeepOrInvest.html'
            keepOrInvest.form_model = models.Player
            keepOrInvest.form_fields = ['keep_or_invest','time_spent']
            return {
                 'assigned_group': self.player.assigned_group,
                'p1': "After considering the rules of this game, please decide whether to invest the initial sum or not. To remind you, these are your options:",
                    'p2_1': "Keep the",
                    'p2_2': "5.",
                    'p3_1': "Invest the $5 with a 50% chance of receiving $8 and a 50% chance of receiving",
                    'p3_2': "2",'dbg':self.player.assigned_group}
        elif self.player.assigned_group == 'C1' or self.player.assigned_group =='C2':
            keepOrInvest.template_name = 'regret/KeepOrInvest.html'
            keepOrInvest.form_model = models.Player
            keepOrInvest.form_fields = ['keep_or_invest','time_spent']
            return {
                'assigned_group': self.player.assigned_group,
                'p1': "After considering the rules of this game, please decide whether to invest the initial sum or not. To remind you, these are your options:",
                'p2_1': "Keep the",
                'p2_2': "5.",
                'p3_1': "Invest the $5 with a 50% chance of receiving $8 and a 50% chance of receiving",
                'p3_2': "2",'dbg':self.player.assigned_group}

class computingChoice(Page):

    template_name = 'regret/preview.html'
    def vars_for_template(self):
        if self.player.assigned_group == 'B1' or self.player.assigned_group == 'B2':
            computingChoice.form_fields = []
            print '*** groups is{} and form_fields is{}***'.format(self.player.assigned_group,computingChoice.form_fields)

            computingChoice.template_name = 'regret/ComputingChoice.html'
            computingChoice.form_fields = []
            return {
                'assigned_group': self.player.assigned_group,
                'p1': "After considering the rules of this game, please decide whether to invest the initial sum or not. To remind you, these are your options:",
                'p2_1': "Keep the",
                'p2_2': "5.",
                'p3_1': "Invest the $5 with a 50% chance of receiving $8 and a 50% chance of receiving",
                'p3_2': "2",'dbg':self.player.assigned_group}
        elif self.player.assigned_group == 'C1' or self.player.assigned_group =='C2':
            #computingChoice.template_name = 'regret/AfterChoice.html'
            if self.player.assigned_group == 'C1':
                if self.player.keep_or_invest == 'Keep':
                    computingChoice.template_name = 'regret/AfterChoice.html'
                    computingChoice.form_fields = []

                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "Your decision and the results are now relayed to your partner.",'dbg':self.player.assigned_group
                    }

                else:

                    computingChoice.template_name = 'regret/Raffle.html'
                    computingChoice.form_model = models.Player
                    computingChoice.form_fields = ['raffle_result','user_written_result']
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "Following your decision to invest the sum, please click on the \"SPIN\" button in order to conduct the raffle.",'dbg':self.player.assigned_group
                    }

            elif self.player.assigned_group == 'C2':
                if self.player.keep_or_invest == 'Keep':
                    computingChoice.template_name = 'regret/Raffle.html'
                    computingChoice.form_model = models.Player
                    computingChoice.form_fields = ['raffle_result','user_written_result']
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "Despite the fact you chose to keep the sum, a raffle will be anyway conducted now. Please click on the \"SPIN\" button",'dbg':self.player.assigned_group
                    }
                else:
                    computingChoice.template_name = 'regret/Raffle.html'
                    computingChoice.form_model = models.Player
                    computingChoice.form_fields = ['raffle_result','user_written_result']
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "Following your decision to invest the sum, please click on the \"SPIN\" button in order to conduct the raffle.",'dbg':self.player.assigned_group
                    }




class showChoice(Page):

    template_name = 'regret/preview.html'


    def vars_for_template(self):
        if self.player.assigned_group == 'B1' or self.player.assigned_group == 'B2':
            showChoice.template_name = 'regret/PartnerChoice.html'
            self.player.should_deduct = bool(random.getrandbits(1))
            #print 'Player should deduct is: {}'.format(self.player.should_deduct)
            if self.player.should_deduct:
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "Your partner has decided to deduct your share.",'dbg':self.player.assigned_group
                    }
            else:
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "Your partner has decided not to deduct your share.",'dbg':self.player.assigned_group}
        elif self.player.assigned_group == 'C1' or self.player.assigned_group == 'C2':
            showChoice.template_name = 'regret/ComputingChoice.html'
            return {'assigned_group': self.player.assigned_group
                    }

class afterChoice(Page):

    template_name = 'regret/preview.html'
    def vars_for_template(self):

        if self.player.assigned_group == 'B1' or self.player.assigned_group == 'B2':
            if self.player.assigned_group == 'B2':
                if self.player.keep_or_invest == 'Keep':
                    afterChoice.template_name = 'regret/Raffle.html'
                    afterChoice.form_model = models.Player
                    afterChoice.form_fields = ['raffle_result','user_written_result']
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "Despite the fact you chose to keep the sum, a raffle will be anyway conducted now. Please click on the \"SPIN\" button",'dbg':self.player.assigned_group
                    }
                else:
                    afterChoice.template_name = 'regret/Raffle.html'
                    afterChoice.form_model = models.Player
                    afterChoice.form_fields = ['raffle_result','user_written_result']
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "Following your decision to invest the sum, please click on the \"SPIN\" button in order to conduct the raffle.",'dbg':self.player.assigned_group
                    }

            if self.player.keep_or_invest == 'Keep':
                afterChoice.template_name = 'regret/AfterChoice.html'
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "Syncing data, please click next.",'dbg':self.player.assigned_group
                }
            else:
                afterChoice.template_name = 'regret/Raffle.html'
                afterChoice.form_model = models.Player
                afterChoice.form_fields = ['raffle_result','user_written_result']
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "Following your decision to invest the sum, please click on the \"SPIN\" button in order to conduct the raffle."
                }
        elif self.player.assigned_group == 'C1' or self.player.assigned_group == 'C2':
            afterChoice.template_name = 'regret/PartnerChoice.html'
            afterChoice.form_fields = []
            self.player.should_deduct = bool(random.getrandbits(1))
            # print 'Player should deduct is: {}'.format(self.player.should_deduct)
            if self.player.should_deduct:
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "Your partner has decided to deduct your share.",
                    'dbg': self.player.assigned_group
                }
            else:
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "Your partner has decided not to deduct your share.",'dbg':self.player.assigned_group}

class expSummery(Page):

    template_name = 'regret/ExperimentSummery.html'

    def vars_for_template(self):
        self.player.total_time = time.time() - self.player.start_time
        if self.player.assigned_group == 'B2':
             # we raffled in any case
            if self.player.should_deduct:
                if self.player.keep_or_invest == 'Keep':
                    self.player.payoff = 1.5
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct your share, you final payoff for the game is : $1.5. Adding the participation fee, your final payoff is $1.5 + $1 = $2.5"
                    }
                else:  # HE WANTED TO INVESTTTT, now check if win or not
                    if self.player.raffle_result == "Win":
                        self.player.payoff = 3
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct your share, your final payoff for the game is: $3. Adding the participation fee, your final payoff is $1 + $3 = $4."
                        }
                    else:
                        self.player.payoff = 0
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct your share, your final payoff for the game is: 0 cent. Adding the participation fee, your final payoff is $0 + $1 = $1"
                        }
            else:  # no deduct
                if self.player.keep_or_invest == 'Keep':
                    self.player.payoff = 2.5
                    return {
                        'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $2.5. Adding the participation fee, your final payoff is $1 + $2.5 = $3.5"
                    }
                else:  # investmenttt
                    if self.player.raffle_result == "Win":
                        self.player.payoff = 4
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $4. Adding the participation fee, your final payoff is $4 + $1 = $5"
                        }
                    else:  # no deduct not win
                        self.player.payoff = 1
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $1. Adding the participation fee, your final payoff is $1 + $1 = $2"
                        }
        elif self.player.assigned_group == 'C2':
            if self.player.should_deduct:
                if self.player.keep_or_invest == 'Keep':
                    self.player.payoff = 1.5
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After integrating your partner's decision to deduct your share, you final payoff for the game is: $1. Adding the participation fee, your final payoff is $1 + $1 = $2"
                    }
                else:  # HE WANTED TO INVESTTTT, now check if win or not
                    if self.player.raffle_result == "Win":
                        self.player.payoff = 3
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct your share, your final payoff for the game is: $4. Adding the participation fee, your final payoff is $3 + $1 = $4."
                        }
                    else:
                        self.player.payoff = 0
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct your share, your final payoff for the game is: 0 cent. Adding the participation fee, your final payoff is 0 + $1 = $1"
                        }
            else:  # no deduct
                if self.player.keep_or_invest == 'Keep':
                    self.player.payoff = 2.5
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $2.5. Adding the participation fee, your final payoff is $1 + $2.5 = $3.5."
                    }
                else:  # investmenttt
                    if self.player.raffle_result == "Win":
                        self.player.payoff = 4
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $4. Adding the participation fee, your final payoff is $4 + $1 = $5."
                        }
                    else:  # no deduct not win
                        self.player.payoff = 1
                        return {
                            'assigned_group': self.player.assigned_group,
                            'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $1. Adding the participation fee, your final payoff is $1 + $1 = $2"
                        }
        if self.player.keep_or_invest == 'Keep':
            if self.player.should_deduct:
                self.player.payoff = 1.5
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "After integrating your partner's decision to deduct your share, your final payoff for the game is $1.5. Adding the participation fee, your final payoff is $1 + $1.5 = $2.5"
                }
            else:  # shouldn't deduct C2 we chose keep
                self.player.payoff = 2.5
                return {
                    'assigned_group': self.player.assigned_group,
                    'p1': "After integrating your partner's decision not to deduct your share, your final payoff for the game is $2.5. Adding the participation fee, your final payoff is $1 + $2.5 = $3.5"
                }
        else:  # we raffled
            if self.player.should_deduct:
                if self.player.raffle_result == "Win":
                    self.player.payoff = 3
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct you share, your final payoff for the game is: $3. Adding the participation fee, your final payoff is $1 + $3 = $4"
                    }
                else:
                    self.player.payoff = 0
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After receiving the results of the raffle and integrating your partner's decision to deduct you share, your final payoff for the game is: 0 cent. Adding the participation fee, your final payoff is 0 + $1 = $1"
                    }
            else:
                if self.player.raffle_result == "Win":
                    self.player.payoff = 4
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $4. Adding the participation fee, your final payoff is $4 + $1 = $5"
                    }
                else:
                    self.player.payoff = 1
                    return {
                        'assigned_group': self.player.assigned_group,
                        'p1': "After receiving the results of the raffle and integrating your partner's decision not to deduct your share, your final payoff for the game is: $1. Adding the participation fee, your final payoff is $1 + $1 = $2"
                    }




'''
<li>{{ p1 }}</li>

<li>
    <li>{{ first_choice }} {{keep_amm}}<span class="glyphicon glyphicon-usd" aria-hidden="true"></span></li>
    <li>{{second_choice}} {{win_amm}}<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
    {{second_choice_cont}}</li>
</li>
<li>{{choice_summery}}</li>
'''

class Results(Page):
    template_name = 'regret/ExperimentSummery.html'

    def vars_for_template(self):
        return{'p1':"ILI STOP HERE PLZ",'dbg':self.player.assigned_group}

class Welcome(Page):
    template_name = 'regret/Welcome.html'

    def vars_for_template(self):
        print 'The group is: {}'.format(self.player.assigned_group)
        return{'assigned_group':self.player.assigned_group}


class Demo(Page):
    template_name = 'regret/Demogrphic.html'
    form_model = models.Player
    form_fields = ['age','income','states','gender','education','language']
    def vars_for_template(self):
        return{'p1':"ILI STOP HERE PLZ",'dbg':self.player.assigned_group}

class End(Page):
    template_name = 'regret/EndSurvey.html'
    form_model = models.Player
    form_fields = ['why_choice','why_deduct','user_comments','satisfaction','wanted_partner','wanted_to_change_invest_or_keep']
    def vars_for_template(self):
        return{'p1':"ILI STOP HERE PLZ",'dbg':self.player.assigned_group,
               'assigned_group': self.player.assigned_group,
               'keep_or_invest':self.player.keep_or_invest,
               'should_deduct': self.player.should_deduct}

class LookForPartner(Page):
    template_name = 'regret/LookingForPartner.html'
    

page_sequence = [
    Welcome,
    Demo,
    intro,
    chooseInvestor,
    LookForPartner,
    roleExplained,
    startingMoney,
    choiceExplanations,
    choiceCont,
    choiceContTwo,
    keepOrInvest,
    computingChoice,
    showChoice,
    afterChoice,
    expSummery,
    End

]


