# -*- coding: utf-8 -*-
# <standard imports>
from __future__ import division

import random

import otree.models
from otree.db import models
from otree import widgets
from otree.common import Currency as c, currency_range, safe_json
from otree.constants import BaseConstants
from otree.models import BaseSubsession, BaseGroup, BasePlayer
from pprint import pprint
import random
# </standard imports>
import operator
author = 'Your name here'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'regret'
    players_per_group = None
    num_rounds = 1
    participants = None

    @staticmethod
    def assignRandomGroup():
        if Constants.participants is None:
            print 'none detected, resetting'
            Constants.participants = [0,0,0,0]
        else:
            print 'constants is not none! printing them'
            for curr in Constants.participants:
                print curr



class Subsession(BaseSubsession):

    def assign_player_to_group(self,pleya):
        #print (vars(pleya))
        groups = self.session.config['participants_in_groups']  # should contain an array of {int,int,int,int}
        #max_in_grp = self.session.config['max_in_sub_grp']
        max_in_grp = max(groups.iteritems(), key=operator.itemgetter(1))[0]
        players = self.session.config['max_in_sub_grp']
        currParts = self.session.config['curr_parts']
        #first get random group
        '''
        grp_key_list = random.sample(groups,1)
        grp_key = grp_key_list[0]
        curr = groups.get(grp_key)
        print 'The key is {} and its value is {}'.format(grp_key,curr)
        currTmp = curr + 1
        groups[grp_key] = currTmp
        print '{} was changed to {}'.format(grp_key,currTmp)
        '''
        keep_looking = True
        total_itt = players

        while (keep_looking):
            print 'currently on player number {}'.format(pleya.id_in_group)
            grp_key_list = random.sample(groups, 1)
            grp_key = grp_key_list[0]
            print '{} was raffled'.format(grp_key)
            curr = groups.get(grp_key)
            print 'Current grp is {} and the biggest grp is {}'.format(grp_key, max_in_grp)
            if grp_key != max_in_grp:
                currTmp = curr + 1
                currParts= currParts+1
                groups[grp_key] = currTmp
                print '{} was changed to {}'.format(grp_key, currTmp)
                pleya.assigned_group = grp_key
                pleya.number_in_group = currTmp
                keep_looking = False
                '''
                currTmp = curr + 1
                groups[grp_key] = currTmp
                print '{} was changed to {}'.format(grp_key, currTmp)
                pleya.assigned_group = grp_key
                pleya.number_in_group = currTmp
                keep_looking = False
                '''
            else:

                '''
                groups_status = self.session.config['group_is_full']
                groups_status[grp_key] = True
                # now get all groups, we need to see if all groups are full
                if sum(groups_status.values()) == 4:
                    keep_looking = False
                    pleya.assigned_group = 'none'
                '''





    def before_session_starts(self):

        #self.assign_player_to_group()
        list_of_players = self.get_players()
        for curr_player in list_of_players:
            self.assign_player_to_group(curr_player)
        #(vars(self.session))

        '''
        arrs = self.get_subsessions()
        for curr in arrs:
            print curr
        '''
        #self.constants.assignRandomGroup()
    pass



class Group(BaseGroup):
    '''
    amount_offered = models.CurrencyField()
    offer_accepted = models.BooleanField()
    number_in_group = models.IntegerField()
    assigned_group = models.CharField()
    '''
    pass

'''
class GroupB(BaseGroup):
    amount_offered = models.CurrencyField()
    offer_accepted = models.BooleanField()

class GroupC(BaseGroup):
    amount_offered = models.CurrencyField()
    offer_accepted = models.BooleanField()

class GroupD(BaseGroup):
    amount_offered = models.CurrencyField()
    offer_accepted = models.BooleanField()
'''

class Player(BasePlayer):
    amount_offered = models.CurrencyField()
    start_time = models.FloatField()
    total_time = models.FloatField()
    offer_accepted = models.BooleanField()
    number_in_group = models.IntegerField()
    should_deduct = models.BooleanField()
    assigned_group = models.CharField()
    user_written_result = models.CharField()
    time_spent = models.CharField()
    why_choice = models.TextField(widget=widgets.Textarea())
    why_deduct = models.TextField(widget=widgets.Textarea())
    user_comments = models.TextField(widget=widgets.Textarea())
    
    
    raffle_result = models.CharField()
    age = models.IntegerField(verbose_name="What is your age?")
    gender = models.CharField(choices=['Male','Female'], verbose_name="What is your gender?")
    language = models.CharField(choices=['English', 'Spanish','Itlian','Other'],
                                verbose_name="What is your primary language?")
    keep_or_invest = models.CharField(
                        choices=['Keep','Invest'])
    states = models.CharField(choices=['Alaska', 'Alabama', 'Arkansas', 'American Samoa', 'Arizona', 'California',
                                       'Colorado', 'Connecticut', 'District of Columbia', 'Delaware', 'Florida',
                                       'Georgia', 'Guam', 'Hawaii', 'Iowa', 'Idaho', 'Illinois', 'Indiana', 'Kansas',
                                       'Kentucky', 'Louisiana', 'Massachusetts', 'Maryland', 'Maine', 'Michigan',
                                       'Minnesota', 'Missouri', 'Northern Mariana Islands', 'Mississippi', 'Montana',
                                       'North Carolina', 'North Dakota', 'Nebraska', 'New Hampshire', 'New Jersey',
                                       'New Mexico', 'Nevada', 'New York', 'Ohio', 'Oklahoma', 'Oregon',
                                       'Pennsylvania',
                                       'Puerto Rico',
                                       'Rhode Island',
                                       'South Carolina',
                                       'South Dakota',
                                       'Tennessee',
                                       'Texas',
                                       'Utah',
                                       'Virginia',
                                       'Virgin Islands',
                                       'Vermont',
                                       'Washington',
                                       'Wisconsin',
                                       'West Virginia',
                                       'Wyoming'
                                       ],verbose_name="Which state do you consider your primary residence?"
                              )
    education = models.IntegerField(choices=[
        (1, 'Some high school or less'),
        (2, 'High school graduate / GED'),
        (3, 'Some college'),
        (4, 'College graduate'),
        (5, 'Graduate degree')],
        verbose_name='What is the highest level of education you have completed?',
        widget=widgets.RadioSelectHorizontal())
    satisfaction = models.IntegerField(choices=[
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
        (6, '6'),
        (7, '7')],
        verbose_name='How satisfied are you from the game?',
        widget=widgets.RadioSelectHorizontal())
    wanted_partner = models.BooleanField(verbose_name='In retrospect, would you have preferred to be assigned to the role of "partner"?')
    wanted_to_change_invest_or_keep = models.BooleanField(
        verbose_name='In retrospect, had you been given the opportunity, would you have decided differently regarding the investment option and kept the sum/chose to invest the sum?')
    income = models.IntegerField(choices=[
        (1, 'Under $15,000'),
        (2, '$15,000 – $25,000'),
        (3, '$25,000 - $35,000'),
        (4, '$35,000 - $50,000'),
        (5, '$50,000 - $75,000'),
        (6, '$75,000 - $100,000'),
        (7, '$100,000 - $150,000'),
        (8, '$150,000 - $200,000'),
        (9, 'Over $200,000')],
        verbose_name='What is your yearly household income?',
        widget=widgets.RadioSelectHorizontal())
    #pass